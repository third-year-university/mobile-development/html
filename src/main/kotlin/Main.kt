import java.io.File

data class Message(var myAddress: String?, var topic: String?, var body: String?, var closing: String?, var sender: String?) {
    init {
        myAddress = myAddress?.let { makeBr(it) }
        topic = topic?.let { makeBr(it) }
        body = body?.let { makeBr(it) }
        closing = closing?.let {makeBr(it) }
        sender = sender?.let { makeBr(it) }
    }
    companion object {
        private fun makeBr(msg: String): String {
            return msg.replace("\n", "<br>")
        }
    }
    fun toHTML(): String {
        return  "<style>" +
                "@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');" +
                "body{font-family: 'Roboto', sans-serif;}" +
                "td{padding: 5px;}" +
                "</style>" +
                "<table bordercolor=\"black\" border=\"1px\" cellspacing\n=\"0px\">" +
                (sender?.let { "<tr><td>Отправитель: </td><td>$it</td></tr> \n" }?: "") +
                (myAddress?.let { "<tr><td>Адрес отправителя: </td><td>$it</td></tr> \n" }?: "") +
                (topic?.let { "<tr><td>Тема: </td><td bgcolor= \"yellow\">$it</td></tr> \n" }?: "") +
                (body?.let { "<tr><td>Сообщение: </td><td> $it" + (closing?.let {"<br><br>$it"}?:" ")   + "</td></tr> \n" }?: "") +
                "</table>"
    }
}


fun main() {

    val messages = arrayOf(
        Message("ceo@babroflot.ru", "Направление Иркутск - Патая (Тайланд)",
        "Жду данные по востребованности маршрута, не позднее 29 октября.",
        "Никита Долбак\nГенеральный директор ПАО \"Баброфлот\"\n664001, г. Иркутск, Ширямово 13Б, оф. 34",
        "Долбак Никита Петрович (Генеральный директор)"
        ),
        Message(null, "Направление Иркутск - Патая (Тайланд)",
            "Жду данные по востребованности маршрута, не позднее 29 октября.",
            null,
            "Долбак Никита Петрович (Генеральный директор)"
        )
    )
    for (i in messages.indices){
        File("letter_$i.html").writeText(messages[i].toHTML())
    }
}